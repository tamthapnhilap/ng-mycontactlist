# MyContactList

Main modules:

- Auth: Login,Logout,Register,ForgotPassword, ChangePassword, ChangeEmail
- Mobile Number Contacts
- Email Contacts
- Skype Contacts
- Facebook Contacts
- Use JSONServer As Mockup For Development Purpose

Advance modules:

- Learn to work with firebase
- Unify contacts.
- Allow to store your contacts as an excel on drive to backup
- Allow import contacts from drive

Eg:

```javascript
contact = {
    "firstName": "Son"
    "lastName": "Nguyen Minh",
    "dob": "01/05/1988",
    "phonenumber": "",
    "email":"",
    "skype":"",
    "facebook":""
}
```

## Summary

1. Modular Architecture

- https://medium.com/@cyrilletuzi/architecture-in-angular-projects-242606567e40
- https://medium.com/@cyrilletuzi/understanding-angular-modules-ngmodule-and-their-scopes-81e4ed6f7407
- https://proandroiddev.com/intro-to-app-modularization-42411e4c421e
- https://angular.io/guide/ngmodules



## Daily

- https://netbasal.com/understanding-angular-structural-directives-659acd0f67e

1.Routing(3 days):

- https://www.smashingmagazine.com/2018/11/a-complete-guide-to-routing-in-angular/
- https://angular.io/tutorial/toh-pt5
- https://angular.io/guide/router
- https://angular.io/guide/lazy-loading-ngmodules

**Structures**

- https://github.com/mathisGarberg/angular-folder-structure

```html
main.ts
core.ts ( Core Module )


```

2. Compare with ReactRoute(2 days):

- https://reacttraining.com/react-router/web/guides/quick-start

## External Knowledge

1.Stripe

- https://stripe.com/docs/api/tokens/create_card
- https://www.youtube.com/watch?v=EildM6OMcoQ
- https://stripe.com/docs/stripe-js
- https://stripe.com/docs/connect/destination-charges


```
charge = stripe.Charge.create(
  amount=1000,
  currency="usd",
  source="tok_visa",
  destination={
    "account": "{CONNECTED_STRIPE_ACCOUNT_ID}",
  }
)
```


here `tok_visa` was created from patient bank account info actually from front-end (`https://stripe.com/docs/stripe-js` or `https://stripe.com/docs/checkout`) when patient enter their info, submit form send data to Stripe server, then Stripe created and return that token. Stripe recommend to create` source="tok_visa",` in this way for safest. So we have 2 options:
- Create Stripe `source` in front-end, when we get to `source`, passing it Odoo Server API to create Stripe `charge` with `amount`, `currency`, `source`, `practitioner_id`
- Or we get patient input info, create Stripe `card` or Stripe `bank account` -> create Stripe `source` -> create Stripe `charge`
