import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Component } from '@angular/core';

import { RouterModule, Routes } from '@angular/router'

@Component({
  selector: 'app-example',
  template: `
    <h1>{{title}}</h1>
  `
})
class ExampleComponent {
  title = 'Example Component'
  public constructor(private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title)
  }
}

@Component({
  selector: 'app-home',
  template: `
    <h1>{{title}}</h1>
  `
})
class HomeComponent {
  title = "Home Component"
  public constructor(private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title)
  }
}

@Component({
  selector: 'app-notfound',
  template: `
    <h1>{{title}}</h1>
  `
})
class NotFoundComponent {
  title = "404 Not Found Component"
  public constructor(private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title)
  }
}

const routes: Routes = [ // top to bottom
  // path : is a string that uses the route matcher DSL(Domain Specific Language)
  // pathMatch: is a string that specifies the matching strategy
  // matcher: define a custom strategy for patch matching and supersedes "path" and "pathMatth"
  // component : is a component type
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "example",
    component: ExampleComponent
  },
  {
    path: '', //default
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**', //wildcard
    component: NotFoundComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
    ExampleComponent
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule
    RouterModule.forRoot(routes)
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
